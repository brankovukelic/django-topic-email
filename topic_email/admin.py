from django.contrib import admin

from .models import Topic


class TopicAdmin(admin.ModelAdmin):
    list_display = (
        'topic',
        'email'
    )
    search_fields = (
        'topic',
        'email'
    )


admin.site.register(Topic, TopicAdmin)
from distutils.core import setup

setup(
    name='django-topic-email',
    version='0.0.1',
    packages=['topic_email', 'topic_email.migrations'],
    license='BSD',
    author='Branko Vukelic',
    author_email='branko@brankovukelic.com',
    description=open('README.rst').read(),
    url='https://bitbucket.org/brankovukelic/django-topic-email',
    download_url='https://bitbucket.org/brankovukelic/django-topic-email/downloads',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Framework :: Django',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
    ]
)
from django.conf import settings

TOPICS = getattr(settings, 'TOPICS', tuple())
DEFAULT_TOPIC_RECIPIENTS = getattr(
    settings,
    'DEFAULT_TOPIC_RECIPIENTS',
    settings.MANAGERS
)
EMAIL_SETTINGS = getattr(settings, 'EMAIL_SETTINGS', {})

# Inherit the default settings form here standard Django settings if not
# specified
if not 'default' in EMAIL_SETTINGS:
    EMAIL_SETTINGS['default'] = {
        'default': {
            'host': getattr(settings, 'EMAIL_HOST', ''),
            'username': getattr(settings, 'EAMIL_HOST_USER', ''),
            'password': getattr(settings, 'EMAIL_HOST_PASSWORD', ''),
            'port': getattr(settings, 'EMAIL_PORT', 25),
            'use_tls': getattr(settings, 'EMAIL_USE_TLS', False),
        }
    }

from __future__ import unicode_literals

from django.test import TestCase

from mock import patch, Mock

from topic_email import email


class PlainText(TestCase):
    def test_conversion_works(self):
        self.maxDiff = None
        html = open('tests/example.html').read()
        text = """Lorem ipsum dolor sit amet, CONSECTETUR adipiscing elit. Cras tincidunt
turpis id risus dictum euismod. Pellentesque habitant morbi tristique
senectus et netus et malesuada fames ac turpis egestas. Nullam erat velit,
gravida adipiscing orci sit amet, tincidunt sodales augue. Nullam hendrerit
euismod justo eu ornare. Nullam eget augue quis enim pellentesque
vestibulum euismod quis neque. Pellentesque egestas mollis lectus, a
scelerisque arcu porttitor in. Interdum et malesuada fames ac ante ipsum
primis in faucibus. In auctor bibendum dui vitae viverra. Nulla sed dictum
justo, eget rutrum quam. Curabitur quam elit, placerat a nunc ac, dignissim
feugiat sapien.

Nam ac odio sit amet justo ultricies tempus. Vestibulum volutpat urna ac
convallis feugiat. Curabitur varius magna quis quam luctus varius non ut
risus. Duis eget tortor at turpis viverra cursus. Quisque in metus
condimentum odio pellentesque aliquet hendrerit eget risus. Pellentesque ac
lobortis erat, tincidunt gravida nisi. Vestibulum lacus purus, fringilla
eget semper sit amet, consectetur vitae ligula. Maecenas tristique dapibus
sem, eu rutrum mauris tincidunt id. In *lacinia pretium risus*.

* list item 1

* list item 2

* list item 3

1. list item 1

2. list item 2

3. list item 3

---------------------------------------------------------------------------

From Lorem Ipsum website <http://www.lipsum.com/>
Contact us <test@example.com>
www.lipsum.com"""
        self.assertEqual(email.to_plain_text(html), text)


class TemplateMessage(TestCase):
    @patch('topic_email.email.to_plain_text')
    @patch('topic_email.email.render_to_string')
    def test_message_from_template_works(self, render, to_plain):
        text, html = email.create_message_body('email_test.html', {
            'title': 'Title',
            'message': 'This is a message',
            'sender': 'Postman'
        })
        self.assertTrue(render.called)
        self.assertTrue(to_plain.called)
        self.assertEqual(html, render.return_value)
        self.assertEqual(text, to_plain.return_value)

    @patch('topic_email.email.to_plain_text')
    @patch('topic_email.email.render_to_string')
    def test_message_from_template_text_only(self, render, to_plain):
        text, html = email.create_message_body('email_test.html', {
            'title': 'Title',
            'message': 'This is a message',
            'sender': 'Postman'
        }, text_only=True)
        self.assertTrue(render.called)
        self.assertFalse(to_plain.called)
        self.assertEqual(text, render.return_value)
        self.assertEqual(html, None)


class Message(TestCase):
    @patch('topic_email.email.EmailMultiAlternatives')
    def test_message_creation_basically_works(self, EMA):
        msg = email.create_message(
            'subject',
            'text',
            'from_email@example.com',
            ['recipient1@example.com'],
            {'header1': 'header1 value', 'header2': 'header2 value'},
            'html'
        )
        self.assertTrue(EMA.called,
                        'EmailMultiAlternatives should be called')
        self.assertTrue(EMA.return_value.attach_alternative.called,
                        'Should attach HTML')
        self.assertEqual(EMA.return_value, msg)

    @patch('topic_email.email.EmailMultiAlternatives')
    def test_message_creation_without_html(self, EMA):
        email.create_message(
            'subject',
            'text',
            'from_email@example.com',
            ['recipient1@example.com'],
            {'header1': 'header1 value', 'header2': 'header2 value'},
        )
        self.assertFalse(EMA.return_value.attach_alternative.called,
                         'Should not attach HTML')


class SendMessage(TestCase):
    def test_sending_basically_works(self):
        msg = Mock()
        email.send_message(msg)
        self.assertTrue(msg.send.called,
                        "Message's send method should be called")


class Everything(TestCase):
    @patch('topic_email.email.create_message_body')
    @patch('topic_email.email.create_message')
    @patch('topic_email.email.send_message')
    def test_send_html_message_basically_works(self, send, cmsg, cbody):
        cbody.return_value = ('text', 'html')
        email.send_mail(
            'subject',
            'sender@example.com',
            'recipient@example.com',
            'template.html',
            {'data': 'value'}
        )
        self.assertTrue(cmsg.called)
        self.assertTrue(cbody.called)
        self.assertTrue(send.called)

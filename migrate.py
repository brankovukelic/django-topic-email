import sys

from django.conf import settings
from django.core.management import call_command

if not settings.configured:
    settings.configure(
        ROOT_URLCONF='',
        DEBUG=False,
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.sqlite3',
                'NAME': 'test.db'
            }
        },
        INSTALLED_APPS=(
            'south',
            'topic_email',
        )
    )

if __name__ == "__main__":
    call_command('schemamigration', 'topic_email',
                 initial=len(sys.argv) > 1,
                 auto=len(sys.argv) == 0)
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _

from .settings import TOPICS
from .email import send_mail


class Topic(models.Model):
    topic = models.CharField(
        max_length=10,
        help_text=_('Name of the topic (up to 10 characters)'),
        choices=TOPICS
    )
    email = models.EmailField(
        help_text=_('Email address for the topic')
    )

    @classmethod
    def send_mail(cls, topic, *args, **kwargs):
        topic_emails = Topic.objects.filter(topic=topic).all()
        addresses = [t.email for t in topic_emails]
        kwargs['to'] = addresses
        send_mail(*args, **kwargs)

    def __unicode__(self):
        return '%s <%s>' % (self.topic, self.email)